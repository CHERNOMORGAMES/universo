extends Node2D


onready var status = get_node("ParallaxBackground/Status")


func _ready():
	Net.connect("connection_failed", self, "_on_connection_failed")
	Net.connect("connection_succeeded", self, "_on_connection_success")
	Net.connect("server_disconnected", self, "_on_server_disconnect")
	Net.connect("players_updated", self, "update_players_list")
	Net.my_name = Global.login
	Net.connect_to_server()


# Обработчик сигнала "connection_succeeded"
func _on_connection_success():
	status.text = "Connected"
	status.modulate = Color.green


# Обработчик сигнала "connection_failed"
func _on_connection_failed():
	status.text = "Connection Failed, trying again"
	status.modulate = Color.red


# Обработчик сигнала "server_disconnected"
func _on_server_disconnect():
	status.text = "Server Disconnected, trying to connect..."
	status.modulate = Color.red
	

# Обработчик сигнала "players_updated"
func update_players_list():
	pass
#	print(String(Net.get_player_list()))
#	$Panel/Players.text = String(gamestate.get_player_list())
