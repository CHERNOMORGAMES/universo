extends WindowDialog


const QueryObject = preload("skriptoj/queries.gd")

var ItemListContent = []


func _ready():
	var q = QueryObject.new()
	
	$HTTPResursojRequestFind.request(q.URL, q.headers, false, 2, q.resurso_query())
	popup_centered()


func FillItemList():
	# Заполняет список найдеными продуктами
	if ItemListContent.size() > 0:
		for ItemID in range(ItemListContent.size()):
			get_node("ItemList").add_item(ItemListContent[ItemID], null, true)
